# javascript-xor

"For boolean expressions x and y, xor (x, y) is true if and only if one of x or y
is true. Otherwise, if x and y are both true or both false, xor returns false."

Example Usage:

- `jsXOR.xor(true, false) // Returns true`
- `jsXOR.xor(false, false) // Returns false`

### Installing this package:

`npm i --save javascript-xor`

### Requiring this package:

`const jsXOR = require('javascript-xor')`

(jsXOR can be any name you want)

### Usage Example:

`jsXOR.xor(true, false)`

### Uninstalling this package:

`npm uninstall --save javascript-xor`

### To Do:

1. Testing on different types of arguments.

2. Better/proper testing.

3. number of supplied arguments check

4. help() function or string

```
// console.log(xor(1, true))
// console.log(xor(0, true))
// console.log(xor(-1, false))
// console.log(xor(null, false))
// console.log(xor(undefined, false))
// console.log(xor('test', false))
// console.log(xor('test', true))

etc.
```

5. Finding real world use cases for this package.
